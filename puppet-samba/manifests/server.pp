# Class: samba::server
#
# Samba server.
#
# For all main options, see the smb.conf(5) and samba(7) man pages.
# For the SELinux related options, see smbd_selinux(8).
#
# Sample Usage :
#  include samba::server
#
class samba::server (
  # Main smb.conf options
  $workgroup                = 'MYGROUP',
  $server_string            = 'Samba Server Version %v',
  $netbios_name             = '',
  $interfaces               = [],
  $hosts_allow              = [],
  $log_file                 = '/var/log/samba/log.%m',
  $max_log_size             = '10000',
  $domain_master            = false,
  $domain_logons            = false,
  $local_master             = undef,
  $security                 = 'user',
  $map_to_guest             = undef,

  $realm                    = undef,

  $guest_account            = undef,
  $os_level                 = undef,
  $preferred_master         = undef,
  $extra_global_options     = [],
  $shares                   = {},



  $winbind_enum_users       = undef,
  $winbind_enum_groups      = undef,
  $winbind_use_default_domain = undef,
  $encrypt_passwords        = undef,
  
  $idmap_config_backend   = undef,
  $idmap_config_range     = undef,
  $idmap_config_workgroup_backend = undef,
  $idmap_config_workgroup_schema_mode = undef,
  $idmap_config_workgroup_range = undef,

  $winbind_nss_info         = undef,
  $winbind_trusted_domains_only = undef,
  $vfs_objects              = undef,
  $map_acl_inherit          = undef,
  $store_dos_attributes     = undef,
  $log_level                = undef,



  # home
  $home_comment             = 'Home Directories',
  $home_path                = '/srv/samba/home',
  $home_read_only           = 'no',
  $home_browseable          = undef,
  #scratch
  $scratch_comment          = undef,
  $scratch_path             = '/srv/samba/scratch',
  $scratch_browseable       = undef,
  $scratch_writable         = undef,
  $scratch_guest_ok         = undef,
  $scratch_available        = undef,

) inherits ::samba::params {

  # Main package and service
  package { $::samba::params::package: ensure => installed }
  service { $::samba::params::service:
    ensure    => running,
    enable    => true,
    hasstatus => true,
    subscribe => File[$::samba::params::config_file],
  }

  file { $::samba::params::config_file:
    require => Package[$::samba::params::package],
    content => template('samba/smb.conf.erb'),
  }
  file { '/srv/samba':
    ensure => directory,

  }
  file {'/srv/samba/home':
    ensure => directory,
    require => File['/srv/samba'],

  }
 
  file {'/srv/samba/scratch':
    ensure => directory,
    require => File['/srv/samba'],

  }


}

